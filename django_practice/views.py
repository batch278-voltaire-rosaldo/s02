from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.http import HttpResponse

from django.contrib.auth.models import User

from django.contrib.auth import authenticate

from django.utils import timezone

from .models import GroceryItem

from django.template import loader

def index(request):
    groceryitem_list = GroceryItem.objects.all()
    template = loader.get_template("index.html")
    context = {
		'groceryitem_list': groceryitem_list
	}
    return HttpResponse(template.render(context, request))
    # return HttpResponse("Hello from the views.py file!")

def groceryitem(request, groceryitem_id):
    response = f"You are viewing the details of {groceryitem_id}"
    return HttpResponse(response)